var CORRECT_RESPONSE = false;
var CURRENT_QUESTION = 1;
var NUMBER_OF_QUESTIONS = 0;
var ANSWERED_QUESTIONS = 1;
var ALL_QUESTIONS_TODAY = 1;
var CURRENT_URI = '/api/v1/question?format=json';
var SOUNDS = true;

// Questions -------------------------------------------------
var Option = React.createClass({
    getInitialState: function(){
        return {
            correct_response:undefined,
        };
    },

    handleClick: function(){
        if(this.props.bundle.its_right == 1){
            this.positiveSound();
            this.setState({correct_response: 1});
            setTimeout(function(){this.nextQuestion()}.bind(this), 1500);
            return true;
        }

        this.negativeSound();
        this.setState({correct_response: 0});
    },

    positiveSound: function(){
        this.playSound("/static/enem/sounds/positive.wav");
    },

    negativeSound: function(){
        this.playSound("/static/enem/sounds/negative.wav");
    },

    playSound: function(src){
        if(SOUNDS){
            var audioElement = document.createElement('audio');
            audioElement.setAttribute('src', src);
            audioElement.setAttribute('autoplay', 'autoplay');

            $.get();

            audioElement.addEventListener("load", function() {
                audioElement.play();
            }, true);
        }
    },


    setProgressBar:function(){
        var progress_bar = $('#bar-green');
        var width = parseInt((ANSWERED_QUESTIONS*100) / NUMBER_OF_QUESTIONS);
        progress_bar.css('opacity', 1).css('width', width + '%');

        var current_question = $('#current-question');
        if(NUMBER_OF_QUESTIONS > ANSWERED_QUESTIONS){
            ++ANSWERED_QUESTIONS;
            current_question.html(ANSWERED_QUESTIONS);
        }else{
            current_question.css('color', '#FFF');
            $('.skill-icon').css('background-color', '#A0C23F');
        }
      },

    nextQuestion: function(){
        this.setProgressBar();

        // Divs
        var actual = $('#' + CURRENT_QUESTION);
        var next = actual.next();

        actual.hide();
        next.show();

        $('html').animate({ scrollTop: 0 });

        if(next.length > 0){

            if(next[0].id == "checkout"){
                var pclass = 'p' + ALL_QUESTIONS_TODAY;
                $('#goal-circle').addClass(pclass);
            }

            $('#total-questions-today').html(ALL_QUESTIONS_TODAY);
            CURRENT_QUESTION = parseInt(next[0].id);
            ALL_QUESTIONS_TODAY++;

        }
      },
    render: function() {
      var url = '#' + this.props.question_id;
      var answer_class = "answer ";

      if(this.state.correct_response !== undefined){
        var state = (this.state.correct_response==1) ? ' true' : 'false'
        answer_class = answer_class + state;
      }


      return (
        <div className="col-md-offset-10 col-sm-offset-12 ">
            <a href={url} onClick={this.handleClick} data-value={this.props.bundle.its_right}>
                <div className={answer_class} >{this.props.bundle.text}</div>
            </a>
        </div>
      );
  }
});
var Checkout = React.createClass({
    restart: function(){

        this.resetProgressBar();
        this.loading();
        this.resetConstants();
        this.addQuestions();
    },

    next: function(){
        CURRENT_URI = this.props.data.meta.next;

        this.resetProgressBar();
        this.loading();
        this.resetConstants();
        this.addQuestions();
    },

    addQuestions: function(){
        ReactDOM.render(
          <QuestionBox url={CURRENT_URI} />,
          document.getElementById('questions')
        );
    },

    resetConstants: function(){
        ANSWERED_QUESTIONS = 1;
    },

    resetProgressBar:function(){
        var progress_bar = $('#bar-green');
        progress_bar.css('opacity', 0).css('width', '0%');

        var current_question = $('#current-question');
        current_question.html(1);
        current_question.css('color', '#A0C23F');
        $('.skill-icon').css('background-color', '#FFF');
    },

    loading: function(){
        $('#questions').html(
            '<div id="loading" style="margin: 0 auto; max-width:50%; text-align: center;">' +
                '<p>Aguarde enquanto selecionando as questões pra você.</p>' +
            '</div>'
        );
    },

    render: function(){
        return(
            <div id="checkout" className="row checkout " style={{'display': 'none '}} >
                <div className="row">
                    <div className="col-md-12 question text-center">
                        <h2>Lição concluída!</h2>
                        <p>Siga em frente para cumprir sua meta diária e aumentar seus conhecimentos!</p>
                        <div className="col-md-offset-8 col-sm-offset-12 margin">
                            <div className="col-md-6 col-sm-12 separator">
                                <div className="c100 orange p100">
                                    <span id="total-questions-today" className="orange">20</span>
                                    <span className="desc">exercícios<br />feitos</span>
                                    <div className="slice">
                                        <div className="bar"></div>
                                        <div className="fill"></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-sm-12">
                                <div id="goal-circle" className="c100 blue">
                                    <span className="blue">100</span>
                                    <span className="desc">meta de<br />exercícios</span>
                                    <div className="slice">
                                        <div className="bar"></div>
                                        <div className="fill"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-offset-12">
                            <button onClick={this.restart} >Praticar de novo</button>
                            <button onClick={this.next} className="true" >Próxima lição</button>
                        </div>
                    </div>
                </div>
            </div>
        );

    }
});
var Question = React.createClass({
    getImage: function(){
        if(this.props.question.image){
            var url = this.props.question.image;
            var alt = 'Imagem na questão ' + this.props.question.id + ' ' + this.props.question.notebook.name + ' ' + this.props.question.notebook.color;
            return (

                <div className="col-md-6 col-sm-12">
                    <figure>
                        <img src={url} alt={alt} />
                    </figure>
                </div>
			)
        }
    },
    render: function() {
        var options_list = this.props.question.options.map(
            function(option){
                if("id" in option){
                    return <Option key={option.id} question_id={this.props.question.id} bundle={option} />
                }
            },
        this);

        var style = {'display':'none'};
        var str_class = "row"
        if(this.props.current == 0){
            style = {'display':'block'};
             str_class = str_class + " first";
        }

        return (
            <div className={str_class} style={style} id={this.props.question.id} >
                <div className="col-md-12 question">
                    <h1 style={{'color': '#000'}}>Questão {this.props.question.id}</h1>
                    <h2 style={{'color': 'rgb(188, 185, 185)', 'margin-top':'8px'}} >{this.props.question.matter.name}</h2>
                    <h3 style={{'color': 'rgb(224, 218, 218)'}}>{this.props.question.notebook.name} {this.props.question.notebook.color}</h3>
                    <br />
                    <p>{this.props.question.enunciation}</p>
                </div>

                {this.getImage()}

                {options_list}
            </div>
        );
    }
});
var QuestionBox = React.createClass({
  getInitialState: function() {return {questions: [], data: []};},
  loadQuestionsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({questions: data.objects, data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  setNumberOfQuestions(){
    $("#number-of-questions").html(this.state.questions.length);
  },

  componentDidMount: function() { this.loadQuestionsFromServer();},
  render: function() {
    NUMBER_OF_QUESTIONS = this.state.questions.length;
    this.setNumberOfQuestions();
    var questions_list = this.state.questions.map(
        function(question, current){
            if("id" in question){
                if(current == 0){
                    CURRENT_QUESTION = question.id
                }
                return <Question current={current} key={question.id} question={question} />
            }
        }
    );

    return (
      <div className="questionBox">
        {questions_list}
        <Checkout data={this.state.data} />
      </div>
    );
  }
});

// PROGRESS BAR ----------------------------------------------
var ProgressBar = React.createClass({
    render: function(){
         return (
         <div className="progress-bar-dynamic">
            <div id="bar-green" className="bar-green" style={{'width' : '0%', 'opacity' : '0'}}></div>
            <div className="skill-icon-container">
                <div className="challenge-number-container">
                    <span id="current-question" className="challenge-step-num">01</span>
                    <span id="number-of-questions" className="challenge-num">20</span>
                    <span className="challenge-num-circle"></span>
                    <span className="challenge-num-backing"></span>
                </div>
                <span className="skill-icon" style={{'position' : 'absolute'}}>
                </span>
            </div>
            <span className="backing"></span>
        </div>
        );
    }
});

// Sounds Control --------------------------------------------
var SoundControl = React.createClass({
    getInitialState: function(){
        return { url: 'static/enem/img/activated-sounds-icon.png'}
    },
    control: function(){
        SOUNDS = !SOUNDS;
        if(SOUNDS){
            this.setState({url:'static/enem/img/activated-sounds-icon.png'})
        }else{
            this.setState({url:'static/enem/img/disabled-sounds-icon.png'})
        }

        return null;
    },

    render: function(){
         return (
             <a href='javascript:void()' onClick={this.control} >
                <span className="profile-avatar">
                    <img src={this.state.url} alt="Sons ativados" />
                </span>
            </a>
        );
    }
});

// ReactDom Elements -----------------------------------------
ReactDOM.render(
  <SoundControl />,
  document.getElementById('profile')
);

ReactDOM.render(
  <ProgressBar />,
  document.getElementById('progress-bar')
);

ReactDOM.render(
  <QuestionBox url={CURRENT_URI} />,
  document.getElementById('questions')
);

