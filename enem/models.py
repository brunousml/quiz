from __future__ import unicode_literals

from django.db import models

from quiz import settings

RIGHT_CHOICES = (('0', 'incorrect'), ('1', 'correct'))


class Notebook(models.Model):
    name = models.CharField(max_length=100)
    color = models.CharField(max_length=30)
    publish_date = models.DateField()

    def __unicode__(self):
        return self.name


class Matter(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class Question(models.Model):
    enunciation = models.TextField()
    notebook = models.ForeignKey(Notebook, blank=True, null=True)
    matter = models.ForeignKey(Matter, blank=True, null=True)
    image = models.ImageField(upload_to='media/enem/questionImage',
                              default='',
                              blank=True,
                              null=True)

    def __unicode__(self):
        return "#" + unicode(self.id) + " - " + self.notebook.name


class Options(models.Model):
    text = models.CharField(max_length=255)
    question = models.ForeignKey(Question, blank=True, null=True)
    its_right = models.CharField(max_length=1, choices=RIGHT_CHOICES, blank=True, null=True)

    def __unicode__(self):
        return self.text
