from django.contrib import admin
from .models import Notebook, Options, Question, Matter

admin.site.register(Notebook)
admin.site.register(Options)
admin.site.register(Question)
admin.site.register(Matter)

