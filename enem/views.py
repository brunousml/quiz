from django.http import HttpResponse
from django.shortcuts import render


def questions(request):
    return render(request, 'enem/index.html')


def start_page(request):
    return render(request, 'enem/start_page.html')
