from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.authentication import Authentication

from .models import Question, Options, Matter, Notebook
from tastypie.resources import ModelResource


class OptionsResource(ModelResource):
    class Meta:
        queryset = Options.objects.all()
        resource_name = 'options'
        always_return_data = True
        authorization = Authorization()
        Authentication = Authentication()


class MatterResource(ModelResource):
    class Meta:
        queryset = Matter.objects.all()
        resource_name = 'Matter'
        always_return_data = True
        authorization = Authorization()
        Authentication = Authentication()


class NotebookResource(ModelResource):
    class Meta:
        queryset = Notebook.objects.all()
        resource_name = 'notebook'
        always_return_data = True
        authorization = Authorization()
        Authentication = Authentication()


class QuestionResource(ModelResource):
    options = fields.ToManyField(OptionsResource, attribute=lambda bundle: Options.objects.filter(question=bundle.obj),
                                 null=True, full=True)
    matter = fields.ToOneField(MatterResource, attribute='matter', null=True, full=True)
    notebook = fields.ToOneField(NotebookResource, attribute='notebook', null=True, full=True)

    class Meta:
        queryset = Question.objects.all()
        resource_name = 'question'
        always_return_data = True
        authorization = Authorization()
        Authentication = Authentication()

    def dehydrate(self, bundle):
        return bundle
