from django.conf.urls import url, include
from . import views

from .api import QuestionResource, OptionsResource
from tastypie.api import Api

v1_api = Api(api_name='v1')
v1_api.register(QuestionResource())
v1_api.register(OptionsResource())

urlpatterns = [
    url(r'^$', views.start_page, name='start_page'),
    url(r'^questions$', views.questions, name='questions'),
    url(r'^api/', include(v1_api.urls)),
    url(r'^api/', include(v1_api.urls)),
]
